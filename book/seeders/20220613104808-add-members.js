'use strict';

module.exports = {
    async up(queryInterface, Sequelize) {
        /**
         * Add seed commands here.
         *
         * Example:
         * await queryInterface.bulkInsert('People', [{
         *   name: 'John Doe',
         *   isBetaMember: false
         * }], {});
         */

        const now = new Date()

        await queryInterface.bulkInsert('members', [{
                code: "M001",
                name: "Angga",
                created_at: now,
                updated_at: now
            },
            {
                code: "M002",
                name: "Ferry",
                created_at: now,
                updated_at: now
            },
            {
                code: "M003",
                name: "Putri",
                created_at: now,
                updated_at: now
            },
        ], {});
    },

    async down(queryInterface, Sequelize) {
        /**
         * Add commands to revert seed here.
         *
         * Example:
         * await queryInterface.bulkDelete('People', null, {});
         */
    }
};