'use strict';

module.exports = {
    async up(queryInterface, Sequelize) {
        /**
         * Add seed commands here.
         *
         * Example:
         * await queryInterface.bulkInsert('People', [{
         *   name: 'John Doe',
         *   isBetaMember: false
         * }], {});
         */
        const now = new Date()

        await queryInterface.bulkInsert('borrows', [{
                member_id: 1,
                book_id: 1,
                return_status: false,
                created_at: now,
                updated_at: now
            },
            {
                member_id: 1,
                book_id: 2,
                return_status: false,
                created_at: now,
                updated_at: now
            },
        ], {});
    },

    async down(queryInterface, Sequelize) {
        /**
         * Add commands to revert seed here.
         *
         * Example:
         * await queryInterface.bulkDelete('People', null, {});
         */
    }
};