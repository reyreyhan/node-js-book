var express = require('express');
var router = express.Router();
const booksController = require('../controllers/books/index')


router.get('/', booksController.showBooks)

module.exports = router