var express = require('express');
var router = express.Router();
const membersController = require('../controllers/members/index')


router.get('/', membersController.showMembers)
router.post('/borrow-book', membersController.borrowBook)
router.post('/return-book', membersController.returnBook)

module.exports = router