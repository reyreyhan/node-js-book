'use strict';

module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.addColumn('borrows', 'pinalty', {
            type: Sequelize.BOOLEAN,
            allowNull: false
        }, );
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.removeColumn(
            'borrows',
            'pinalty'
        );
    }
};