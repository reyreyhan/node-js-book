'use strict';

module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable('borrows', {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false
            },
            member_id: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            book_id: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            return_status: {
                type: Sequelize.BOOLEAN,
                default: false,
                allowNull: false
            },
            created_at: {
                type: Sequelize.DATE,
                allowNull: false
            },
            updated_at: {
                type: Sequelize.DATE,
                allowNull: false
            }
        }).then(() => queryInterface.addConstraint('borrows', {
            type: 'foreign key',
            name: 'BORROWS_MEMBER_ID',
            fields: ['member_id'],
            references: {
                table: 'members',
                field: 'id'
            }
        })).then(() => queryInterface.addConstraint('borrows', {
            type: 'foreign key',
            name: 'BORROWS_BOOK_ID',
            fields: ['book_id'],
            references: {
                table: 'books',
                field: 'id'
            }
        })).then(() => queryInterface.addIndex('borrows', ['id']))

    },

    async down(queryInterface, Sequelize) {
        await queryInterface.dropTable('borrows');
    }
};