'use strict';

module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable('members', {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false
            },
            code: {
                type: Sequelize.STRING,
                allowNull: false
            },
            name: {
                type: Sequelize.STRING,
                allowNull: false
            },
            created_at: {
                type: Sequelize.DATE,
                allowNull: false
            },
            updated_at: {
                type: Sequelize.DATE,
                allowNull: false
            }
        }).then(() => queryInterface.addIndex('members', ['id']))
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.dropTable('members');
    }
};