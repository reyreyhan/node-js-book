const Validator = require('fastest-validator')
const v = new Validator()

const { Borrow } = require('../../models')
const { Member } = require('../../models')
const { Book } = require('../../models')

module.exports = async(req, res) => {

    // validate input 
    const schema = {
        member_code: 'string|empty:false',
        book_code: 'string|empty:false'
    }

    const validate = v.validate(req.body, schema)

    if (validate.length) {
        return res.status(400).json({
            status: 'error',
            message: validate,
            data: null
        })
    }

    // check member on database
    const member = await Member.findOne({
        where: {
            code: req.body.member_code
        }
    })

    if (!member) {
        return res.status(404).json({
            status: 'error',
            message: 'member not found',
            data: null
        })
    }

    // check book on database
    const book = await Book.findOne({
        where: {
            code: req.body.book_code
        }
    })

    if (!book) {
        return res.status(404).json({
            status: 'error',
            message: 'book not found',
            data: null
        })
    }

    // check borrowed user & borrowed book
    const borrowed = await Borrow.findOne({
        where: {
            member_id: member.id,
            book_id: book.id,
            return_status: false
        }
    })

    if (!borrowed) {
        return res.status(404).json({
            status: 'error',
            message: 'borrowed book not found',
            data: null
        })
    }

    let pinaltyStatus = false

    let borrowedDate = new Date(borrowed.createdAt).getTime()
    let dateNow = new Date().getTime()
    const diffDay = (dateNow - borrowedDate) / (1000 * 3600 * 24)

    if (diffDay > 3) {
        pinaltyStatus = true
    }

    await borrowed.update({
        return_status: true,
        pinalty: pinaltyStatus

    })

    await book.update({
        stock: book.stock + 1
    })

    return res.status(200).json({
        status: 'success',
        message: 'success return book',
        data: borrowed
    })



}