const { Member } = require('../../models')
const { Borrow } = require('../../models')

module.exports = async(req, res) => {
    // show member with borrowed book
    let members = await Member.findAll({
        attributes: ['id', 'code', 'name'],
        include: {
            model: Borrow,
            as: 'borrows',
        }
    })

    let checkReturnStatus = 0;

    members.forEach(element => {
        element.dataValues.borrows.forEach(element => {
            if (element.dataValues.return_status == false) {
                checkReturnStatus += 1
            }
        })

        element.dataValues.total_borrowed_books = element.dataValues.borrows.length
        element.dataValues.borrowed_books_now = checkReturnStatus

        delete element.dataValues.borrows
        checkReturnStatus = 0
    });


    return res.status(200).json({
        status: 'success',
        message: 'success get members',
        data: members
    })
}