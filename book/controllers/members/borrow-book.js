const Validator = require('fastest-validator')
const v = new Validator()
const moment = require('moment');

const { Member } = require('../../models')
const { Book } = require('../../models')
const { Borrow } = require('../../models')

module.exports = async(req, res) => {
    // validate input 
    const schema = {
        member_code: 'string|empty:false',
        book_code: 'string|empty:false'
    }

    const validate = v.validate(req.body, schema)

    if (validate.length) {
        return res.status(400).json({
            status: 'error',
            message: validate,
            data: null
        })
    }

    // check member on database
    const member = await Member.findOne({
        where: {
            code: req.body.member_code
        }
    })

    if (!member) {
        return res.status(404).json({
            status: 'error',
            message: 'member not found',
            data: null
        })
    }

    // check book on database
    const book = await Book.findOne({
        where: {
            code: req.body.book_code
        }
    })

    if (!book) {
        return res.status(404).json({
            status: 'error',
            message: 'book not found',
            data: null
        })
    }

    // check stock / book availability
    if (book.stock < 1) {
        return res.status(404).json({
            status: 'error',
            message: 'all book allready borrowed',
            data: null
        })
    }

    // check member, maximum borrow 2 books
    const memberBorrow = await Borrow.findAll({
        where: {
            member_id: member.id,
            return_status: false
        }
    })

    if (memberBorrow.length >= 2) {
        return res.status(404).json({
            status: 'error',
            message: `member ${member.code}: ${member.name} already borrow ${memberBorrow.length} book`,
            data: null
        })
    }

    // check pinalty member
    const memberPinalty = await Borrow.findOne({
        where: {
            member_id: member.id,
            return_status: true,
            pinalty: true
        }
    })

    if (memberPinalty) {
        let datePinalty = new Date(memberPinalty.updatedAt).getTime()
        let dateNow = new Date().getTime()
        const diffDay = (dateNow - datePinalty) / (1000 * 3600 * 24)

        // update status pinalty because more than 3 days
        if (diffDay > 3) {

            await memberPinalty.update({
                pinalty: false
            })

        } else {
            return res.status(404).json({
                status: 'error',
                message: `member ${member.code}: ${member.name} have pinalty`,
                data: null
            })
        }

    }

    // borrow book & update book stock
    let bookStock = await Book.findOne({
        where: {
            code: req.body.book_code
        }
    })

    await bookStock.update({
        stock: bookStock.stock - 1
    })

    const borrowBookData = {
        member_id: member.id,
        book_id: book.id
    }

    const borrowBook = await Borrow.create(borrowBookData)


    return res.status(200).json({
        status: 'success',
        message: 'success borrow book',
        data: borrowBook
    })
}