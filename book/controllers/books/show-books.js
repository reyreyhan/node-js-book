const { Op } = require('sequelize')
const { Book } = require('../../models')

module.exports = async(req, res) => {
    // show available book
    const books = await Book.findAll({
        attributes: ['id', 'code', 'title', 'author', 'stock'],
        where: {
            stock: {
                [Op.gt]: 0
            }
        }
    })

    return res.status(200).json({
        status: 'success',
        message: 'success get books',
        data: books
    })

}