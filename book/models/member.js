module.exports = (sequelize, DataTypes) => {
    const Member = sequelize.define('Member', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false
        },
        code: {
            type: DataTypes.STRING,
            allowNull: false
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        createdAt: {
            field: "created_at",
            type: DataTypes.DATE,
            allowNull: false
        },
        updatedAt: {
            field: "updated_at",
            type: DataTypes.DATE,
            allowNull: false
        }
    }, {
        tableName: 'members',
        timestamps: true
    })

    Member.associate = function(models) {
        // associations can be defined here
        Member.hasMany(models.Borrow, {
            as: 'borrows',
            sourceKey: 'id',
            foreignKey: 'member_id'
        })
    }

    return Member
}