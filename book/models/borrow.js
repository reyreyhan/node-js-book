module.exports = (sequelize, DataTypes) => {
    const Borrow = sequelize.define('Borrow', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false
        },
        member_id: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        book_id: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        return_status: {
            type: DataTypes.BOOLEAN,
            default: false
        },
        pinalty: {
            type: DataTypes.BOOLEAN,
            default: false
        },
        createdAt: {
            field: "created_at",
            type: DataTypes.DATE,
            allowNull: false
        },
        updatedAt: {
            field: "updated_at",
            type: DataTypes.DATE,
            allowNull: false
        }
    }, {
        tableName: 'borrows',
        timestamps: true
    })

    Borrow.associate = function(models) {
        // associations can be defined here
        Borrow.belongsTo(models.Book, {
            foreignKey: 'book_id',
            as: 'book'
        })

        Borrow.belongsTo(models.Member, {
            foreignKey: 'member_id',
            as: 'member'
        })
    }

    return Borrow
}