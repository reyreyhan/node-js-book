module.exports = (sequelize, DataTypes) => {
    const Book = sequelize.define('Book', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false
        },
        code: {
            type: DataTypes.STRING,
            allowNull: false
        },
        title: {
            type: DataTypes.STRING,
            allowNull: false
        },
        author: {
            type: DataTypes.STRING,
            allowNull: false
        },
        stock: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        createdAt: {
            field: "created_at",
            type: DataTypes.DATE,
            allowNull: false
        },
        updatedAt: {
            field: "updated_at",
            type: DataTypes.DATE,
            allowNull: false
        }
    }, {
        tableName: 'books',
        timestamps: true
    })

    return Book
}