## Run Directory algoritma
```
cd algoritma
node namafile.js
```

## Run Directory book
```
cd book
npm install
setting .env
npx sequelize db:migrate
npx sequelize db:seed:all
nodemon
```

### Postman Documentation
```
https://documenter.getpostman.com/view/3058701/UzBgupTe
{{url}} = http://localhost:3000/
```
