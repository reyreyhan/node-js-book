// const word = "~EIGEN~1DSACX!SA!~4aaaV"
const word = "EIGEN1"
let wordTemp = []
let wordFinal = ""

for (let i = 0; i < word.length; i++) {
    if ((/[a-zA-Z]/).test(word[i]) == true) {
        wordTemp += word[i]
    } else {
        if (wordTemp.length != 0) {
            wordFinal += wordTemp.split("").reverse().join("") + word[i]
            wordTemp = []
        } else {
            wordFinal += word[i]
        }
    }
}

if (wordTemp.length != 0) {
    wordFinal += wordTemp.split("").reverse().join("")
}

console.log(wordFinal)