// const input = ['xc', 'dz', 'bbb', 'dz', 'var', 'dump', 'dd', 'dd', 'js']  
// const query = ['bbb', 'ac', 'dz', 'xixi', 'ok', 'dd', 'js']

const input = ['xc', 'dz', 'bbb', 'dz']
const query = ['bbb', 'ac', 'dz']

let output = []
let tempCountOutput = 0
let words = []

for (let i = 0; i < query.length; i++) {
    tempCountOutput = 0

    for (let j = 0; j < input.length; j++) {
        if (query[i] == input[j]) {
            tempCountOutput += 1
        }
    }

    if (tempCountOutput != 0) {
        words[i] = ` karena kata '${query[i]}' terdapat ${tempCountOutput} pada INPUT`
    } else {
        words[i] = ` kata '${query[i]}' tidak ada dalam input`
    }

    output.push(tempCountOutput)
}

console.log(`[${output}] ${words}`)