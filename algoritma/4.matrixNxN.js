// const matrix = [[1, 2, 0, 3], [4, 5, 6, 5], [7, 8, 9, 7], [10, 11, 12, 9]]
const matrix = [[1, 2, 0], [4, 5, 6], [7, 8, 9]]

let diagonalCounter = 0
let diagonalCounterReduce = matrix.length - 1

let firstDiagonal = "diagonal pertama = "
let secondDiagonal = "diagonal kedua = "
let diagonalResult = "maka hasilnya adalah "

let firstDiagonalResult = 0
let secondDiagonalResult = 0

for (let i = 0; i < matrix.length; i++) {
    if (matrix[i].length != matrix.length) {
        console.log("is not matrix N x N")
        return false
    } else {
        if (diagonalCounter + 1 == matrix.length) {
            firstDiagonal += `${matrix[i][diagonalCounter]} = `
            secondDiagonal += `${matrix[i][diagonalCounterReduce]} = `
        } else {
            firstDiagonal += `${matrix[i][diagonalCounter]} + `
            secondDiagonal += `${matrix[i][diagonalCounterReduce]} + `
        }

        firstDiagonalResult += matrix[i][diagonalCounter]
        diagonalCounter++

        secondDiagonalResult += matrix[i][diagonalCounterReduce]
        diagonalCounterReduce--

    }
}

console.log(firstDiagonal + firstDiagonalResult)
console.log(secondDiagonal + secondDiagonalResult)
console.log(`${diagonalResult} ${firstDiagonalResult} - ${secondDiagonalResult} = ${firstDiagonalResult - secondDiagonalResult}`)